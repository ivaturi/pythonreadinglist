 
# My python reading list #

## Beginner stuff ##

Articles and blog posts 

  * [The most important Python docs](http://www.secnetix.de/olli/Python/doclinks.hawk)
  * [A primer on Python metaclasses](https://jakevdp.github.io/blog/2012/12/01/a-primer-on-python-metaclasses/)


Books
  
  * [Learn Python the hard way](https://learnpythonthehardway.org/book/)
  * [(Wikibook) Python programming](https://en.wikibooks.org/wiki/Python_Programming)
  * [Dive into Python](http://www.diveintopython.net/toc/index.html)


